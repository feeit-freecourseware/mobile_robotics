#!/bin/bash

# Get the absolute directory path of the script
project_dir=$(dirname "$(readlink -f "$0")")

# Initialize flags
build_flag=false
cuda_flag=false
push_flag=false
dev_flag=false  # we are developing now, don't interrupt with pulling

# Function to display help
show_help() {
    echo "Usage: $0 [OPTIONS]"
    echo ""
    echo "Options:"
    echo "  --build     Build the image. The image will not be run."
    echo "  --push      Push the built image to the registry. The image will not be run now."
    echo "  --cuda      Spin a container with CUDA setup."
    echo "  --dev       Work with the local image. Do not pull from the registry."
    echo "  --help      Show this help message."
    exit 0
}

# Loop through all arguments
for arg in "$@"; do
    case "$arg" in
        build) build_flag=true ;;
        push) push_flag=true ;;
        --cuda) cuda_flag=true ;;
        --dev) dev_flag=true ;;
        --help) show_help ;;
    esac
done

# Set container and image name
container_name="mobile_robotics"
image_name="registry.gitlab.com/feeit-freecourseware/mobile_robotics:latest"

if $build_flag; then
    echo "Building an image..."
    podman build -t "$image_name" -f "$dockerfile_name" "$project_dir"
    exit 0
fi

if $push_flag; then
    echo "Pushing the image to GitLab registry..."
    podman push "$image_name"
    exit 0
fi

if ! $dev_flag; then
    echo "Pulling the latest image..."
    podman pull "$image_name"
fi

echo "Spinning a new container..."
if $cuda_flag; then
    podman run -it --rm --replace -p 8888:8888 -v "$project_dir:/app:Z" --name "$container_name" --net=host --privileged --env=DISPLAY=$DISPLAY --device nvidia.com/gpu=all "$image_name"
else
    podman run -it --rm --replace -p 8888:8888 -v "$project_dir:/app:Z" --name "$container_name" "$image_name"
fi
