FROM fedora:41

RUN dnf install -y Xvfb gcc gcc-c++ python3-pip git nodejs swig python3-devel; dnf clean all

RUN pip install --no-cache-dir jupyterlab plotly matplotlib ipywidgets pygame gymnasium gymnasium[all] torch torchvision

ENTRYPOINT ["jupyter", "lab", "--ip=0.0.0.0", "--allow-root", "--no-browser", "--notebook-dir=/app"]

